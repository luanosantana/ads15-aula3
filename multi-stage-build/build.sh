#!/bin/sh
echo Building alexellis2/href-counter:build

TAG="alexellis/href-counter"
VERSION="build"

docker build -t ${TAG}:${VERSION} . -f Dockerfile.build

docker container create --name extract ${TAG}:${VERSION}
docker container cp extract:/go/src/github.com/alexellis/href-counter/app ./app  
docker container rm -f extract

echo Building alexellis2/href-counter:latest

docker build --no-cache -t ${TAG}:latest .
#rm ./app
