
ADS15 Aula 3
---

**Módulo 3** - Gerência de containers com Docker e Kubernetes

1. **ativ2** - Arquivos e resolução via script da atividade 2
2. **multi-stage-build** - Arquivos apresentados durante o tópico de *Multi Stage Build*
3. **kubernetes**  - Arquivos apresentados durante o tópico *conhecendo o kubernetes*
